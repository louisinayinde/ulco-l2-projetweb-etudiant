document.addEventListener('DOMContentLoaded', function () {
    
        // variable signin form
        let form_signin = document.querySelector(".account-signin")
        let lastname = document.querySelector("#name")
        let firstname = document.querySelector("#f_name")
        let password = document.querySelector("#pwd")
        let password_match = document.querySelector("#pwd_2")
        let mail = document.querySelector("#email")
        let bt_submit = document.querySelector("#submit")
        bt_submit.disabled = true

                //variable verification mail
        let verif_mail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/


        
                //verification nom de famille
        lastname.addEventListener('keyup', () =>{
                if(lastname.value.length <2 ) {
                        lastname.setAttribute('class', 'invalid')
                        lastname.previousElementSibling.setAttribute('class', 'invalid')
                        lastname.classList.remove("count")
                }
                else {
                        lastname.setAttribute('class', 'valid')
                        lastname.previousElementSibling.setAttribute('class', 'valid')
                        lastname.classList.add("count")
                        if(document.querySelectorAll(".count").length == 5) bt_submit.disabled = false
                }
        })


                //verification prenom
        firstname.addEventListener('keyup', () =>{
                if(firstname.value.length <2 ) {
                        firstname.setAttribute('class', 'invalid')
                        firstname.previousElementSibling.setAttribute('class', 'invalid')
                        firstname.classList.remove("count")
                }
                else {
                        firstname.setAttribute('class', 'valid')
                        firstname.previousElementSibling.setAttribute('class', 'valid')
                        firstname.classList.add("count")
                        if(document.querySelectorAll(".count").length == 5) bt_submit.disabled = false
                }
        })
        

                //verification mail
        mail.addEventListener('keyup', () =>{
                if(!mail.value.match(verif_mail))  {
                        mail.setAttribute('class', 'invalid')
                        mail.previousElementSibling.setAttribute('class', 'invalid')
                        mail.classList.remove("count")
                }
                else {
                        mail.setAttribute('class', 'valid')
                        mail.previousElementSibling.setAttribute('class', 'valid')
                        mail.classList.add("count")
                        if(document.querySelectorAll(".count").length == 5) bt_submit.disabled = false
                }
        })

                //verification password
        password.addEventListener('keyup', () =>{
                if(password.value.length >=6 && password.value.match(/[0-9]/g) && password.value.match(/[a-z]/g)) {
                        password.setAttribute('class', 'valid') 
                        password.previousElementSibling.setAttribute('class', 'valid')

                        if(password_match.value === password.value){
                                password.setAttribute('class', 'valid')
                                password.previousElementSibling.setAttribute('class', 'valid')
                                //-----//
                                password_match.setAttribute('class', 'valid') 
                                password_match.previousElementSibling.setAttribute('class', 'valid')
                                //-----//
                                password.classList.add("count")
                                if(document.querySelectorAll(".count").length == 4) bt_submit.disabled = false
                        }
                        else{
                                password.setAttribute('class', 'invalid')
                                password.previousElementSibling.setAttribute('class', 'invalid')
                                //-----//
                                password_match.setAttribute('class', 'invalid') 
                                password_match.previousElementSibling.setAttribute('class', 'invalid')
                                //-----//
                                password.classList.remove("count")
                        }
                }
                else  {
                        password.setAttribute('class', 'invalid')
                        password.previousElementSibling.setAttribute('class', 'invalid')
                        password.classList.remove("count")
                }
        })

        //verification password_2
        password_match.addEventListener('keyup', () =>{
                if(password_match.value.length >=6 && password_match.value.match(/[0-9]/g) && password_match.value.match(/[a-z]/g)) {
                        password_match.setAttribute('class', 'valid') 
                        password_match.previousElementSibling.setAttribute('class', 'valid')

                        if(password_match.value === password.value){
                                password.setAttribute('class', 'valid')
                                password.previousElementSibling.setAttribute('class', 'valid')
                                //-----//
                                password_match.setAttribute('class', 'valid') 
                                password_match.previousElementSibling.setAttribute('class', 'valid')
                                //-----//
                                password_match.classList.add("count")
                                if(document.querySelectorAll(".count").length == 4) bt_submit.disabled = false
                        }
                        else{
                                password.setAttribute('class', 'invalid')
                                password.previousElementSibling.setAttribute('class', 'invalid')
                                //-----//
                                password_match.setAttribute('class', 'invalid')
                                password_match.previousElementSibling.setAttribute('class', 'invalid')
                                //-----//
                                password_match.classList.remove("count") 
                        }
                }
                 else {
                         password_match.setAttribute('class', 'invalid')
                         password_match.previousElementSibling.setAttribute('class', 'invalid')
                         password_match.classList.remove("count")
                }
                 
                
        }) 


})


