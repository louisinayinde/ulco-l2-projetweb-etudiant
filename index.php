<?php

session_start();
if (isset($_GET['login'])) {
    $login = $_GET['login'];

     $_SESSION['login'] = $login ;
}
/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

// bonjour, pour la route store il faut creer un nouveau rooter ou garder $rooter?

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');
$router->get('/store', 'controller\StoreController@store');
$router->get('/store/{:num}', 'controller\StoreController@product');
$router->get('/account', 'controller\AccountController@account');
$router->post('/account/login', 'controller\AccountController@login');
$router->post('/account/signin', 'controller\AccountController@signin');
$router->get('/account/logout', 'controller\AccountController@logout');
$router->post('/comment/{:num}', 'controller\CommentController@postComment');
$router->post('/store/search', 'controller\StoreController@search');
$router->get('/account/infos', 'controller\AccountController@infos');
$router->post('/account/update', 'controller\AccountController@update');
$router->get('/cart', 'controller\CartController@cart');
$router->post('/cart/add', 'controller\CartController@add');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');



/** Ecoute des requêtes entrantes *********************************************/

$router->listen();