<?php

namespace model;

class AccountModel {

  static function check($firstname, $lastname, $mail, $password): bool
  {
    if(strlen($lastname) >=2 && !empty($lastname) &&  preg_match("/^([a-zA-Z' ]+)$/",$lastname) && strlen($firstname) >=2 && !empty($firstname) &&  preg_match("/^([a-zA-Z' ]+)$/",$firstname)){
      if(strlen($password) >=6 && !empty($password)){
        if(filter_var($mail, FILTER_VALIDATE_EMAIL) && preg_match("/^[^\W][a-zA-Z0-9\-\._]+[^\W]@[^\W][a-zA-Z0-9\-\._]+[^\W]\.[a-zA-Z]{2,4}$/", $mail)){
          // Connexion à la base de données
          $db = \model\Model::connect();         
          $sql =  "SELECT mail FROM account WHERE mail =  '$mail'";
    
          // Exécution de la requête
          $req = $db->prepare($sql);
          $req->execute();
          $results = $req->fetch();
          if(empty($results)){
            return true;
          }
          else  
            return false;

        }
      }          
    }
    return false;
  }
          

  static function signin($firstname, $lastname, $mail, $password): bool
  {

    // Connexion à la base de données
    $db = \model\Model::connect();
    echo(var_dump($mail));
    if(self::check($firstname, $lastname, $mail, $password)){

      //$sql = "INSERT INTO account (firstname,lastname,mail,password) VALUES ('$firstname','$lastname','$mail','$password')";

      $sql = "INSERT INTO account (firstname, lastname, mail, password) VALUES(?, ?, ?, ?)";
      $req = $db->prepare($sql);
      $req->execute(array($firstname, $lastname, $mail, $password));
      
      return true;
    }
    else
      return false;

  }

  static function login($mail, $password) {
      // Connexion à la base de données
      $db = \model\Model::connect();         
      $sql =  "SELECT id, firstname, lastname, mail, password FROM account WHERE mail='$mail'";

      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute();
      $user = $req->fetch();
      if(password_verify($password, $user['password']) && $user['mail'] == $mail){
        return $user;
      }
       
      else return null;
      
  }


  static function account(): array
  {


    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function update( $firstname, $lastname, $mail, $old_mail): bool
  {
    if(strlen($lastname) >=2 && !empty($lastname) &&  preg_match("/^([a-zA-Z' ]+)$/",$lastname) && strlen($firstname) >=2 && !empty($firstname) &&  preg_match("/^([a-zA-Z' ]+)$/",$firstname)){
      if(filter_var($mail, FILTER_VALIDATE_EMAIL) && preg_match("/^[^\W][a-zA-Z0-9\-\._]+[^\W]@[^\W][a-zA-Z0-9\-\._]+[^\W]\.[a-zA-Z]{2,4}$/", $mail)){
        // Connexion à la base de données
        $db = \model\Model::connect();         
        $sql =  "UPDATE account SET firstname = ?, lastname = ?, mail = ? WHERE mail = ?";
  
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($firstname, $lastname, $mail, $old_mail));
        return true;
        }
        else
          return false;
      }

      
             
    return false;
  }


}


