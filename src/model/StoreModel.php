<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listSearch($category_1, $category_2, $category_3, $order_asc, $order_desc, $search_input): array{

    // Connexion à la base de données
    $db = \model\Model::connect();

    
    // Requête SQL

    if ($search_input !==""){
      $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id WHERE product.name LIKE (?)";
      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute(array($search_input));
    }
    else {
          if($order_asc === 0){
            if($category_1=="NULL" && $category_2=="NULL" && $category_3=="NULL"){
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id ORDER BY price ASC";
                    // Exécution de la requête
                    $req = $db->prepare($sql);
                    $req->execute();
            }
            else {
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id WHERE product.category =(?) OR product.category =(?) OR product.category =(?) ORDER BY price ASC";
                  // Exécution de la requête
                  $req = $db->prepare($sql);
                  $req->execute(array($category_1, $category_2, $category_3));
            }
          }
          else if($order_desc === 1){
            if($category_1=="NULL" && $category_2=="NULL" && $category_3=="NULL"){
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id ORDER BY price DESC";
                // Exécution de la requête
                $req = $db->prepare($sql);
                $req->execute();
            }
            else {
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id WHERE product.category =(?) OR product.category =(?) OR product.category =(?) ORDER BY price DESC";
              // Exécution de la requête
              $req = $db->prepare($sql);
              $req->execute(array($category_1, $category_2, $category_3));
            }
          }
          else{
            if($category_1=="NULL" && $category_2=="NULL" && $category_3=="NULL"){
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id";
                  // Exécution de la requête
                  $req = $db->prepare($sql);
                  $req->execute();
            }
            else {
              $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id WHERE product.category =(?) OR product.category =(?) OR product.category =(?)";
                    // Exécution de la requête
                    $req = $db->prepare($sql);
                    $req->execute(array($category_1, $category_2, $category_3));
            }
          }
    }

    // Retourner les résultats (type array)
    return $req->fetchAll();

  }

  static function listProducts(): array{
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT *, product.name as nameProduct FROM product INNER JOIN category ON product.category = category.id";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function infoProduct(int $id){
    //connexion à la base de données
    $db =  \model\Model::connect();

    //Requete SQL
    $sql= "SELECT  product.id as idProduct, product.name as nameProduct, price, image, image_alt1, image_alt2, image_alt3,spec, category.name as nameCategory FROM product  INNER JOIN  category ON product.category = category.id  WHERE product.id = $id";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetch();
  }


}