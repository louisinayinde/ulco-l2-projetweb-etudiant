<?php

namespace model;

class CommentModel {   


  static function insertComment($comment, $id_user, $id_product): bool
  {

    $id_prod = $id_product;
    $id_account = $id_user;
    $date = date('l jS \of F Y h:i:s A');

    // Connexion à la base de données
    $db = \model\Model::connect();
    if(!empty($comment)){

      //$sql = "INSERT INTO account (firstname,lastname,mail,password) VALUES ('$firstname','$lastname','$mail','$password')";

      $sql = "INSERT INTO comment (content, date, id_product, id_account) VALUES(?, ?, ?, ?)";
      $req = $db->prepare($sql);
      $req->execute(array($comment, $date, $id_prod, $id_account));
      
      return true;
    }
    else
      return false;
  }

  static function listComment(): array
  {

    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT content, firstname, lastname, id_product FROM comment INNER JOIN account ON comment.id_account = account.id";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }


}