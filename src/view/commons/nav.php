<nav>
    <img src="/public/images/logo.jpeg" alt="logo">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if (isset($_SESSION['firstname'])) : ?>
    <a class="account" href="/account/infos" ><img src="/public/images/avatar.png" alt="avatar"> <?= $_SESSION['firstname']." ".$_SESSION['lastname']?></a>
    <a class="account" href="/cart">Panier</a>
    <a class="account" href="/account/logout">Déconnexion</a>
    <?php else : ?>
    <a class="account" href="/account"><img src="/public/images/avatar.png" alt="avatar"> Compte</a>
    <?php endif ?>
</nav>