<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

  <form method="post" action="/store/search">

    <h4>Rechercher</h4>
    <input type="text" name="search" placeholder="Rechercher un produit" />

    <h4>Catégorie</h4>
    <?php $i=1 ?>
    <?php foreach ($params["categories"] as $c) { ?>
      <input type="checkbox" name="category<?= $i ?>" value="<?= $c["name"] ?>" />
      <?= $c["name"] ?>
      <?php $i++ ?>
      <br/>
    <?php } ?>

    <h4>Prix</h4>
    <input type="radio" name="order1" /> Croissant <br />
    <input type="radio" name="order2" /> Décroissant <br />

    <div><input type="submit" value="Appliquer" /></div>

  </form>

  <!-- Affichage des produits --------------------------------------------------->

  <div class="products">

  <?php $i=1 ?>
  <?php foreach ($params["products"] as $c) { ?>
      
        <div class="card">
        <a href="/store/<?= $i ?>">
          <p class="card-image"> <img src="/public/images/<?= $c["image"]?>" alt="image_produit"> </p>
          <p class="card-category"> <?= $c["name"]?></p>
          <p class="card-title"><?= $c["nameProduct"]?></p>
          <p class="card-price"><?= $c["price"]?></p>
          </a>
        </div>
      
    <?php $i++;} ?>

  </div>

</div>
