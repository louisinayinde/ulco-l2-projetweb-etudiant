
<!-- FORMULAIRE -->
<?php if(!isset($_GET['status'])){?>
    <div id="account">

<form class="account-login" method="post" action="/account/login">

    

    <!-- CHAMP FORMULAIRE -->
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">


    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input type="text" name="userlastname" placeholder="Nom" id="name"/>

    <p>Prénom</p>
    <input type="text" name="userfirstname" placeholder="Prénom" id="f_name" />

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" id="email" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd"/>

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd_2" />

    <input type="submit" id="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>

<?php }?>

<!-- -----------------------------------------------------
-----------------------MSG FAILURE------------------------
------------------------------------------------------ -->


<!-- FORUMAIRE + MESSAGE ERREUR CONNEXION -->

<?php if(isset($_GET['status']) && $_GET['status']=="login_failure" ){?>
    <div class="box error" style="justify-content: center" >La connexion a échoué! Vérifiez vos identifiants et réessayez.</div>

    <div id="account">

<form class="account-login" method="post" action="/account/login">

    

    <!-- CHAMP FORMULAIRE -->
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">


    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input type="text" name="userlastname" placeholder="Nom" id="name"/>

    <p>Prénom</p>
    <input type="text" name="userfirstname" placeholder="Prénom" id="f_name" />

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" id="email" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd"/>

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd_2" />

    <input type="submit" id="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
<?php }?>

<!-- FORULAIRE + MESSAGE ERREUR INSCRIPTION -->
<?php if(isset($_GET['status']) && $_GET['status']=="signin_failure"){?>
    <div class="box info" style="justify-content: center">L'inscription a échoué! Veuillez renseigner correcement tous les champs.</br>Utilisez un autre mail si le problème persiste.</div>

    <div id="account">

<form class="account-login" method="post" action="/account/login">

    

    <!-- CHAMP FORMULAIRE -->
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">


    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input type="text" name="userlastname" placeholder="Nom" id="name"/>

    <p>Prénom</p>
    <input type="text" name="userfirstname" placeholder="Prénom" id="f_name" />

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" id="email" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd"/>

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd_2" />

    <input type="submit" id="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
<?php }?>

<!-- -----------------------------------------------------
-----------------------MSG SUCCESS------------------------
------------------------------------------------------ -->

<!-- FORULAIRE + MESSAGE DECONNEXION REUSSIE -->
<?php if(isset($_GET['status']) && $_GET['status']=="logout_success"){?>
    <div class="box info" style="justify-content: center">Vous etes déconnecté. A bientot !</div>

    <div id="account">

<form class="account-login" method="post" action="/account/login">

    

    <!-- CHAMP FORMULAIRE -->
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">


    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input type="text" name="userlastname" placeholder="Nom" id="name"/>

    <p>Prénom</p>
    <input type="text" name="userfirstname" placeholder="Prénom" id="f_name" />

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" id="email" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd"/>

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd_2" />

    <input type="submit" id="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
<?php }?>

<!-- FORULAIRE + MESSAGE INSCRIPTION REUSSIE -->

<?php if(isset($_GET['status']) && $_GET['status']=="signin_success"){?>
    <div class="box info" style="justify-content: center"> Inscription réussie! Vous pouvez dès à présent vous connecter.</div>
    <div id="account">

<form class="account-login" method="post" action="/account/login">

    

    <!-- CHAMP FORMULAIRE -->
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">


    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input type="text" name="userlastname" placeholder="Nom" id="name"/>

    <p>Prénom</p>
    <input type="text" name="userfirstname" placeholder="Prénom" id="f_name" />

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" id="email" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd"/>

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" id="pwd_2" />

    <input type="submit" id="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
<?php }?>
