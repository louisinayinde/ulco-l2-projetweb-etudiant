<?php $c = $params["productInfo"]?>
<?php $g = $params["listComment"]?>

<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $c["image"]?>" alt="image_produit">
            <div class="product-miniatures">
            <div><img src="/public/images/<?= $c["image"]?>" alt="image_produit"></div>
                <?php for($i=1; $i<4 ; $i++):?>
                    <div><img src="/public/images/<?=str_replace(".jpg", "_alt$i.jpg", $c["image"])?>"/></div>
                <?php endfor; ?>
            </div>
        </div>

        <div class="product-infos">
            <p class="product-category"> <?= $c["nameCategory"]?> </p>
            <h1><?= $c["nameProduct"]?></h1>
            <p class="product-price"><?= $c["price"]?></p>
            <form action="">
                <button type="button" id="minus">-</button>
                <button type="button" id="quantity">1</button>
                <button type="button" id="plus">+</button>
                <input type="submit" value="Ajouter au panier">
            </form>
            <div class="box error" style="visibility: hidden;">
                Quantité maximale autorisée !
            </div>
        </div>

    </div>
    <div>
            <div class="product-spec">
                <h2>Spécificités</h2>
                <?= $c["spec"]?>
            </div>

                                                                <!-- ------------------- -->
                                                                <!-- ESPACE COMMENTAIRE  -->
                                                                <!-- ------------------- -->



            <?php if (isset($_SESSION['firstname'])) : ?>
            <!-- partie session -->
            <div class="product-comments">
                <h2>Avis</h2>
                <?php if ($g!=NULL) : ?>
                    <?php foreach ($g as $val):?>
                        <p class="product-comment-author"><?= $val["firstname"]." ".$val["lastname"] ?></p>
                        <p><?= $val["content"]?></p>
                    <?php endforeach; ?>
                <?php else : ?>
                <?php endif; ?>

                <form method="post" action="/comment/<?= $c["idProduct"]?>">

                    <input type="text" name="comment" placeholder="Rédigez un commentaire" style="margin-bottom:1rem;" />
                    <input type="text" name="idProd" value="<?= $c["idProduct"]?>" style="display:none;">
                    <input type="submit" value="Envoyer" />
                </form>
            </div>
            
            <!-- partie visiteur -->
            <?php else : ?>
            
            <div class="product-comments">
                <h2>Avis</h2>
                <?php foreach ($g as $val):?>
                    <p class="product-comment-author"><?= $val["firstname"]." ".$val["lastname"] ?></p>
                    <p><?= $val["content"]?></p>
                <?php endforeach; ?>
            </div>

            <?php endif ?>


             </div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function () {
    
        
    // Pour l'affichage des images
    let disp_img = document.querySelector(".product-images img")
    let disp_miniature = document.querySelectorAll(".product-miniatures img")

    disp_miniature.forEach(element => {
        element.addEventListener('click', () => {
            disp_img.src = element.src
        })
    })



    // Pour les boutons
    let minus_button = document.querySelector("#minus")
    let disp_number = document.querySelector("#quantity")
    let plus_button = document.querySelector("#plus")
    let maxQuantity = document.querySelector(".box")

    minus_button.addEventListener('click', () => {

        if( parseInt(disp_number.innerText) >1 && parseInt(disp_number.innerText) <=5 ) {
            let num = parseInt(disp_number.innerText) -1 
            disp_number.innerText = num.toString()
            maxQuantity.style.visibility ="hidden"
        }

    })

    plus_button.addEventListener('click', () => {

        if(parseInt(disp_number.innerText) >= 1 && parseInt(disp_number.innerText)<5){
            let num = parseInt(disp_number.innerText) +1 
            disp_number.innerText = num.toString()
        }
        else if( parseInt(disp_number.innertext) === 5 ){
            maxQuantity.style.visibility ="visible"
        }

    })


})

</script>
