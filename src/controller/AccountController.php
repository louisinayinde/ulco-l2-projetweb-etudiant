<?php

namespace controller;

class AccountController {

  public function account(): void
  {
    // Variables à transmettre à la vue
    $params = [
      "title"  => "account",
      "module" => "account.php"
    ];

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function login(): void
  {
    // $mail = "";
    // $password = "";

    $mail=$_POST['usermail'];
    $password= $_POST['userpass'];

    $bool_account = \model\AccountModel::login($mail, $password);

    if ($bool_account != NULL) {
      $_SESSION['id'] = $bool_account['id'] ;
      $_SESSION['firstname'] = $bool_account['firstname'] ;
      $_SESSION['lastname'] = $bool_account['lastname'] ;
      $_SESSION['mail'] = $bool_account['mail'] ;
      header('Location: /store');
      exit();
      
    }
    else {
      header("Location: /account?status=login_failure");
      exit();
    }
  }

  public function signin(): void
  {
    $firstname = "";
    $lastname= "";
    $mail = "";
    $password = "";
    
    // vérifier que login n'existe pas
    $firstname=htmlspecialchars($_POST['userfirstname']);
    $lastname=htmlspecialchars($_POST['userlastname']);
    $mail=$_POST['usermail'];
    
    $password= password_hash($_POST['userpass'], PASSWORD_BCRYPT);
    $bool_account = \model\AccountModel::signin( $firstname, $lastname, $mail, $password);

    if ($bool_account == true) {
      header("Location: /account?status=signin_success");
      exit();
    }
    else {
      header("Location: /account?status=signin_failure");
      exit();
    }

    $params = array(
      "title" => "account",
      "module"=>"account.php",
      "bool_account"=>$bool_account
    );
    \view\Template::render($params);
    
    
  }

  public function logout(): void{
    session_destroy(); 
    header("Location: /account?status=logout_success");
    exit();
    

    $params = array(
      "title" => "account",
      "module"=>"account.php"
    );
    \view\Template::render($params);

  }

  public function infos(): void{
    // Variables à transmettre à la vue
    $params = [
      "title"  => "Infos",
      "module" => "infos.php"
    ];

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public static function update():void {
    $firstname=htmlspecialchars($_POST['userfirstname']);
    $lastname=htmlspecialchars($_POST['userlastname']);
    $mail=$_POST['usermail'];
    $old_mail = $_SESSION['mail'];

    $bool_account = \model\AccountModel::update( $firstname, $lastname, $mail, $old_mail);
    var_dump($bool_account);
    if ($bool_account) {
      $_SESSION['firstname'] = $firstname ;
      $_SESSION['lastname'] = $lastname ;
      $_SESSION['mail'] = $mail ;
      header("Location: /account/infos?status=infos_success");
      exit();
    }
    else {
      header("Location: /account/infos?status=infos_failure");
      exit();
    }
  }


}



