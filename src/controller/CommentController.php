<?php

namespace controller;

class CommentController {

  public function postComment(): void
  {
    $comment=$_POST['comment'];
    $id_user = $_SESSION['id'] ;
    $id_product = $_POST['idProd'];

    $bool_comment = \model\CommentModel::insertComment($comment, $id_user, $id_product);

    if (!$bool_comment) {
      header('Location: /store/'.$id_product);
      exit();
    }
    else {
      header('Location: /store');
      exit();
    }
  }

}