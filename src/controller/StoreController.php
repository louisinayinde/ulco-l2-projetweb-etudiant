<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProducts();
    

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "products" => $products
    );
    

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function search(): void
  {
    error_reporting(0);

        // $search
    $search_input = "";
    if($_POST['search']) $search_input = htmlspecialchars($_POST['search']);
    

        // $category_1
    $category_1 ="NULL";
    if($_POST['category1']) $category_1 = "1";
        // $category_2
    $category_2 ="NULL";
    if($_POST['category2']) $category_2 = "2";
        // $category_3 
    $category_3 ="NULL";
    if($_POST['category3']) $category_3 = "3";

    $order_asc=NULL;
    if($_POST['order1']){ $order_asc=0;} 

    $order_desc=NULL;
    if($_POST['order2']){ $order_desc=1;} 

    $categories = \model\StoreModel::listCategories();
    $listSearch = \model\StoreModel::listSearch($category_1, $category_2, $category_3, $order_asc, $order_desc, $search_input);

    $params = array(
      "module" => "store.php",
      "categories" => $categories,
      "products" => $listSearch
    );

    \view\Template::render($params);
  }

  public function product($id):void{
    $productInfo = \model\StoreModel::infoProduct($id);
    $listComment = \model\CommentModel::listComment();

    $params = array(
      "title" => "Product",
      "module" => "product.php",
      "productInfo" => $productInfo,
      "listComment" => $listComment
    );

    if(!$productInfo){
      header('Location: /store');
      exit();
    }

    \view\Template::render($params);

  }

}