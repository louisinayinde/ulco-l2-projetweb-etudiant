<?php

namespace controller;

class CartController {

  public function cart(): void{
    // Variables à transmettre à la vue
    $params = [
      "title"  => "Cart",
      "module" => "cart.php"
    ];

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }



}



